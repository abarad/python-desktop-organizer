import logging
import os
import shutil
import sys

from extensions import *

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

DESKTOP = os.path.expanduser("~/Desktop")
MAPPER = {
    'Images': IMAGES,
    'Text': TEXT,
    'Audio': AUDIO,
    'Videos': VIDEOS,
    'Code': CODE
}


def handle_file(file_name, _type):
    TYPE_DIR = os.path.join(DESKTOP, _type)
    if not os.path.isdir(TYPE_DIR):
        os.mkdir(TYPE_DIR)
    logger.info(f"Transfer '{file_name}' from Desktop to '{TYPE_DIR}'")
    shutil.move(os.path.join(DESKTOP, file_name), os.path.join(TYPE_DIR, file_name))


if __name__ == '__main__':
    desktop_files = os.listdir(DESKTOP)
    for file_name in desktop_files:
        if '.' in file_name:
            extension = file_name.split('.')[-1]
            for _type, extensions in MAPPER.items():
                if extension in extensions:
                    logger.info(f"File '{file_name}' is a '{_type}' type.")
                    handle_file(file_name, _type)
        else:
            logger.info(f'File {file_name} has no extension.')
